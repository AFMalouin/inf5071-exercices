# Infographie

Ce dépôt contient les énoncés des laboratoires du cours INF5071 Infographie,
enseigné à l'UQAM.

* [Exemples](exemples/README.md)
* [Labo 1](labo01/README.md)
* [Labo 2](labo02/README.md)
* [Labo 3](labo03/README.md)

