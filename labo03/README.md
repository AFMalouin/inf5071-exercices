# Laboratoire 3: Animations

## 1 - Animations de base

En utilisant le logiciel Blender, concevez de petites animations simples sur un
cube ou une sphère et qui fait varier les paramètres suivantes:

- La *couleur*;
- La *transparence* (pour faire apparaître et disparaître l'objet);
- Une *texture de déplacement*.

## 2 - Exportation et importation d'une animation

Exportez votre animation sous format DAE et récupérez-la dans le logiciel
Godot. Jouez avec le contrôle d'animation (en anglais, *animation player*).
N'hésitez pas à ajouter des propriétés à animer afin de comparer le
fonctionnement par rapport à Blender.

## 3 - Courbes paramétrées

Dans Blender, concevez une animation dans laquelle un objet se déplace le long
d'une courbe de Bézier ou d'une courbe NURBS.

Ensuite, animez la caméra pour qu'elle suive elle aussi une courbe. Par
exemple, vous pourriez concevoir une caméra qui évolue le long d'un cercle, en
pointant toujours vers l'origine.

Notez que lors de l'exportation/important, Godot reconnaît également les
caméras comme objet, de même que certaines animations contraintes par des
courbes.

## 4 - Système articulé

- Concevez d'abord un modèle simple, dont la géométrie sera suffisamment
  complexe pour en faire un système articulé. Ne faites pas un personnage
  complet, simplement un maillage avec différentes extensions.
- Optionnellement, appliquez-lui une texture, par exemple en coloriant
  les faces à l'aide de couleurs différentes;
- Créez un système articulé de quelques os qui sont alignés avec les
  différentes parties de votre cube;
- Procédez à l'habillage de votre maillage par rapport au système articulé;
- Créez une animation simple utilisant votre système articulé.
- Si vous avez le temps, exportez-la et importez-la dans Godot.
