import bpy
import random

# Cleaning default scene
bpy.ops.object.select_all(action='DESELECT')
bpy.data.objects['Camera'].select = True
bpy.data.objects['Cube'].select = True
bpy.data.objects['Lamp'].select = True
bpy.ops.object.delete()

# Materials
ground_material = bpy.data.materials.new('GroundMaterial')
ground_material.diffuse_color = (0, 0.1, 0)
trunk_material = bpy.data.materials.new('TrunkMaterial')
trunk_material.diffuse_color = (0.3, 0.05, 0)
leaves_material = bpy.data.materials.new('LeavesMaterial')
leaves_material.diffuse_color = (0, 0.5, 0)

# Creating ground
bpy.ops.mesh.primitive_cube_add(location=(0, 0, 0))
bpy.context.active_object.name = 'Ground'
bpy.context.scene.objects.active.scale = (8, 8, 0.1)
bpy.context.active_object.data.materials.append(ground_material)

# Creating trees
locations = [tuple(random.randint(-7, 7) for _ in range(2)) for _ in range(10)]
for (i,(x,y)) in enumerate(locations):
    bpy.ops.mesh.primitive_uv_sphere_add(size=0.8, location=(x,y,2))
    bpy.context.active_object.name = 'Leaves.%s' % i
    bpy.context.active_object.data.materials.append(leaves_material)
    bpy.ops.mesh.primitive_cylinder_add(radius=0.1, location=(x,y,1))
    bpy.context.active_object.name = 'Tree.%s' % i
    bpy.context.active_object.data.materials.append(trunk_material)
    bpy.data.objects['Leaves.%s' % i].select = True
    bpy.ops.object.join()

# Saving the scene
bpy.ops.wm.save_as_mainfile(filepath='scene.blend')
